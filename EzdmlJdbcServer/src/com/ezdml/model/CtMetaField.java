package com.ezdml.model;
/**
 * CtMetaField
 * 数据字段
 * @author User(EMAIL) 2021/2/15 21:39:56
 * @version 1.0
 * Generated by javascript

CtMetaField(数据字段)
-----------------------------------------------
Id(编号)                          PKString
Name(名称)                        String
DisplayName(显示名称)             String
DataType(数据类型)                String
DataTypeName(数据类型)            String
KeyFieldType(关键字段类型)        String
RelateTable(关联表)               String
RelateField(关联字段)             String
IndexType(索引类型)               String
Memo(备注)                        String
DefaultValue(缺省值)              String
Nullable(是否可为空)              Bool
DataLength(最大长度)              Integer
DataScale(精度)                    Integer
OrderNo(排序号)                  Float
*/

import java.util.Map;

/**
 * 数据字段
 */
public class CtMetaField {
	/**
	 * 编号
	 */
	protected int id;
	/**
	 * 名称
	 */
	protected String name;
	/**
	 * 显示名称
	 */
	protected String displayName;
	/**
	 * 数据类型
	 */
	protected int dataType;
	/**
	 * 数据类型
	 */
	protected String dataTypeName;
	/**
	 * 关键字段类型
	 * 1=id 3=rid
	 */
	protected int keyFieldType;
	/**
	 * 关联表
	 */
	protected String relateTable;
	/**
	 * 关联字段
	 */
	protected String relateField;
	/**
	 * 索引类型
	 * 1Unique 2Normal
	 */
	protected int indexType;
	/**
	 * 备注
	 */
	protected String memo;
	/**
	 * 缺省值
	 */
	protected String defaultValue;
	/**
	 * 是否可为空
	 */
	protected boolean nullable;
	/**
	 * 最大长度
	 */
	protected int dataLength;
	/**
	 * 精度 
	 */
	protected int dataScale;
	/**
	 * 排序号
	 */
	protected int orderNo;

	public CtMetaField() {
	}

	public CtMetaField(Map<String, Object> map) {
		this.loadFromMap(map);
	}

	public int getId() {
		return id;
	}

	public void setId(int value) {
		this.id = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String value) {
		this.displayName = value;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int value) {
		this.dataType = value;
	}

	public String getDataTypeName() {
		return dataTypeName;
	}

	public void setDataTypeName(String value) {
		this.dataTypeName = value;
	}

	public int getKeyFieldType() {
		return keyFieldType;
	}

	public void setKeyFieldType(int value) {
		this.keyFieldType = value;
	}

	public String getRelateTable() {
		return relateTable;
	}

	public void setRelateTable(String value) {
		this.relateTable = value;
	}

	public String getRelateField() {
		return relateField;
	}

	public void setRelateField(String value) {
		this.relateField = value;
	}

	public int getIndexType() {
		return indexType;
	}

	public void setIndexType(int value) {
		this.indexType = value;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String value) {
		this.memo = value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String value) {
		this.defaultValue = value;
	}

	public boolean getNullable() {
		return nullable;
	}

	public void setNullable(boolean value) {
		this.nullable = value;
	}

	public int getDataLength() {
		return dataLength;
	}

	public void setDataLength(int value) {
		this.dataLength = value;
	}

	public int getDataScale() {
		return dataScale;
	}

	public void setDataScale(int value) {
		this.dataScale = value;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void reset() {
		id = 0;
		name = null;
		displayName = null;
		dataType = 0;
		dataTypeName = null;
		keyFieldType = 0;
		relateTable = null;
		relateField = null;
		indexType = 0;
		memo = null;
		defaultValue = null;
		nullable = false;
		dataLength = 0;
		dataScale = 0;
		orderNo = 0;
	}

	public void assignFrom(CtMetaField AObj) {
		if (AObj == null) {
			reset();
			return;
		}
		id = AObj.id;
		name = AObj.name;
		displayName = AObj.displayName;
		dataType = AObj.dataType;
		dataTypeName = AObj.dataTypeName;
		keyFieldType = AObj.keyFieldType;
		relateTable = AObj.relateTable;
		relateField = AObj.relateField;
		indexType = AObj.indexType;
		memo = AObj.memo;
		defaultValue = AObj.defaultValue;
		nullable = AObj.nullable;
		dataLength = AObj.dataLength;
		dataScale = AObj.dataScale;
		orderNo = AObj.orderNo;
	}
	
	public void loadFromMap(Map<String,Object> map) {
		if(map==null) {
			reset();
			return;
		}
		id=TypeUtil.ObjToInt(map.get("Id"));
		name=TypeUtil.ObjToStr(map.get("Name"));
		displayName=TypeUtil.ObjToStr(map.get("DisplayName"));
		dataType=TypeUtil.ObjToInt(map.get("DataType"));
		dataTypeName=TypeUtil.ObjToStr(map.get("DataTypeName"));
		keyFieldType=TypeUtil.ObjToInt(map.get("KeyFieldType"));
		relateTable=TypeUtil.ObjToStr(map.get("RelateTable"));
		relateField=TypeUtil.ObjToStr(map.get("RelateField"));
		indexType=TypeUtil.ObjToInt(map.get("IndexType"));
		memo=TypeUtil.ObjToStr(map.get("Memo"));
		defaultValue=TypeUtil.ObjToStr(map.get("DefaultValue"));
		nullable=TypeUtil.ObjectToBool(map.get("Nullable"));
		dataLength=TypeUtil.ObjToInt(map.get("DataLength"));
		dataScale=TypeUtil.ObjToInt(map.get("DataScale"));
		orderNo=TypeUtil.ObjToInt(map.get("OrderNo"));
	}
	
	public void saveToMap(Map<String, Object> map) {
		if(map==null)
			return;
		if(id>0)
			map.put("Id", id);
		if(name!=null)
			map.put("Name", name);
		if(displayName!=null)
			map.put("DisplayName", displayName);
		if(dataType>0)
			map.put("DataType", dataType);
		if(dataTypeName!=null)
			map.put("DataTypeName", dataTypeName);
		if(keyFieldType!=0)
			map.put("KeyFieldType", keyFieldType);
		if(relateTable!=null)
			map.put("RelateTable", relateTable);
		if(relateField!=null)
			map.put("RelateField", relateField);
		if(indexType!=0)
			map.put("IndexType", indexType);
		if(memo!=null)
			map.put("Memo", memo);
		if(defaultValue!=null)
			map.put("DefaultValue", defaultValue);
		map.put("Nullable", nullable);
		if(dataLength>0)
			map.put("DataLength", dataLength);
		if(dataScale>0)
			map.put("DataScale", dataScale);
		if(orderNo!=0)
			map.put("OrderNo", orderNo);
	}
	
	public Map<String, Object> toMap(){
		Map<String, Object> map=new ArrayMap<String, Object>();
		saveToMap(map);
		return map;
	}

}
