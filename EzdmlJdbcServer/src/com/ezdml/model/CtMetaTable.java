/**
 * CtMetaTable
 * 数据表
 * @author User(EMAIL) 2021/2/15 21:39:00
 * @version 1.0
 * Generated by javascript

CtMetaTable(数据表)
-------------------------------------
Id(编号)                  PKString
Name(名称)                String
caption(逻辑名)          String
Memo(备注)                String

MetaFields(字段列表)      List        //<<类名:LI>>

*/
package com.ezdml.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据表
 */
public class CtMetaTable {
	/**
	 * 编号
	 */
	protected int id;
	/**
	 * 名称
	 */
	protected String name;
	/**
	 * 逻辑名
	 */
	protected String caption;
	/**
	 * 备注
	 */
	protected String memo;
	/**
	 * 字段列表
	 */
	protected List<CtMetaField> metaFields=new ArrayList<CtMetaField>();

	public CtMetaTable() {
	}

	public CtMetaTable(Map<String, Object> map) {
		this.loadFromMap(map);
	}

	public int getId() {
		return id;
	}

	public void setId(int value) {
		this.id = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getDescribe() {
		return caption;
	}

	public void setDescribe(String value) {
		this.caption = value;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String value) {
		this.memo = value;
	}

	public List<CtMetaField> getMetaFields() {
		return metaFields;
	}
	
	public CtMetaField newField() {
		CtMetaField res=new CtMetaField();
		this.metaFields.add(res);
		return res;
	}

	public CtMetaField fieldByName(String colName) {
		if(colName==null)
			return null;
		for(CtMetaField fd : this.metaFields)
			if(colName.equalsIgnoreCase(fd.getName()))
				return fd;
		return null;
	}

	public void reset() {
		id = 0;
		name = null;
		caption = null;
		memo = null;

		metaFields.clear();
	}

	public void assignFrom(CtMetaTable AObj) {
		if (AObj == null) {
			reset();
			return;
		}
		id = AObj.id;
		name = AObj.name;
		caption = AObj.caption;
		memo = AObj.memo;
		
		Map<String, Object> map=new HashMap<String, Object>();
		metaFields.clear();
		for(int i=0;i<AObj.metaFields.size();i++) {
			map.clear();
			AObj.metaFields.get(i).saveToMap(map);
			metaFields.add(new CtMetaField(map));
		}
	}

	public void loadFromMap(Map<String,Object> map) {
		if(map==null) {
			reset();
			return;
		}
		id=TypeUtil.ObjToInt(map.get("Id"));
		name=TypeUtil.ObjToStr(map.get("Name"));
		caption=TypeUtil.ObjToStr(map.get("Caption"));
		memo=TypeUtil.ObjToStr(map.get("Memo"));
		
		List<Map<String, Object>> fs=TypeUtil.CastToList_SO(map.get("MetaFields"));
		metaFields.clear();
		for(int i=0;i<fs.size();i++) {
			metaFields.add(new CtMetaField(fs.get(i)));
		}
	}
	
	public void saveToMap(Map<String, Object> map) {
		if(map==null)
			return;
		if(id>0)
			map.put("Id", id);
		if(name!=null)
			map.put("Name", name);
		if(caption!=null)
			map.put("Caption", caption);
		if(memo!=null)
			map.put("Memo", memo);

		List<Map<String, Object>> fs=new ArrayList<Map<String, Object>>();
		for(int i=0;i<metaFields.size();i++) {
			fs.add(metaFields.get(i).toMap());
		}
		Map<String, Object> fmap=new HashMap<String, Object>();
		fmap.put("Count", fs.size());
		fmap.put("items", fs);
		map.put("MetaFields", fmap);
	}
	

	public Map<String, Object> toMap(){
		Map<String, Object> map=new ArrayMap<String, Object>();
		saveToMap(map);
		return map;
	}


}
